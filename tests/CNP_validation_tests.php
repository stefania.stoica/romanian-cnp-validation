<?php
echo "1.  ".isCnpValid('91234567891011')."<br/>";  // length > 13
echo "2.  ".isCnpValid('9123456789')."<br/>";         // length < 13
echo "3.  ".isCnpValid('0123456789101')."<br/>";    // first digit = 0
echo "4.  ".isCnpValid('2#234567a9101')."<br/>";    // only digits
echo "5.  ".isCnpValid('2881325035271')."<br/>";    // invalid date (MM)
echo "6.  ".isCnpValid('2880230035271')."<br/>";    // invalid date (DD)
echo "7.  ".isCnpValid('5220201035271')."<br/>";    // birthday > today
echo "8.  ".isCnpValid('2880725005271')."<br/>";    // JJ = 0
echo "9.  ".isCnpValid('2880725605271')."<br/>";    // invalide JJ
echo "10. ".isCnpValid('2880725030001')."<br/>";    // NNN = 000
echo "11. ".isCnpValid('2880725035272')."<br/>";    // wrong check digit
?>