<?php
// function isCnpValid(string $value): bool
function isCnpValid(string $value){

	$gender   = intval(substr($value,0,1));
	$yy 	      = substr($value,1,2);
	$mm       = substr($value,3,2);
	$dd         = substr($value,5,2);
	$jj           = substr($value,7,2);
	$nnn       = substr($value,9,3);

	// check length = 13, only digits, first digit <> 0
	if ((strlen($value) <> 13)
	or (preg_match('/^[1-9][0-9]*$/', $value) == 0))
	{
	   return false;
	}

	//  check if AALLZZ is a valide date
	switch ($gender) {
	case 1:
	case 2:
	case 7:
	case 8:
	case 9:
	  $yyul = "19".$yy;
	  break;
	case 3:
	case 4:
	  $yyul = "18".$yy;
	  break;
	case 5:
	case 6:
	  $yyul = "20".$yy;
	  break;
	}

	if(!checkdate($mm,$dd,$yyul)) {
	return false;
	}

	// check birthdate <= today (for gender 5,6 who are born in 21th century)
	if (($gender==5 or $gender==6) and (strcmp(date("ymd"),substr($value,1,6)) < 0)){
	return false;
	}

	// check JJ
	$jjul = intval($jj);
	if ($jjul == 0 or $jjul > 52 or ($jjul > 46 and $jjul < 51)){
	return false;
	}

	// NNN <> 0
	if (intval($nnn)== 0){
	return false;
	}

	// check digit
	for ($i = 0; $i <=12; $i++){
	$cnp[] = intval($value[$i]);
	}

	$control = '279146358279';
	$suma = 0;

	for ($i = 0; $i <=11; $i++){
	$suma +=$cnp[$i] * intval($control[$i]);
	}

	$cifra_control = $suma % 11;
	if($cifra_control == 10) {
	$cifra_control = 1;
	}

	if ($cifra_control <>$cnp[12]){
	return false;
	}

	// else CNP is valid
	return true;
}
?>